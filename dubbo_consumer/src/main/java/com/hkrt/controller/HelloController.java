package com.hkrt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hkrt.service.HelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Surpass
 * @Package com.hkrt.controller
 * @Description: ${todo}
 * @date 2020/10/27 16:20
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Reference   //订阅zookeeper，查找相关服务
    private HelloService helloService;


    @ResponseBody
    @RequestMapping("/say")
    public String sayHello(String name){
        System.out.println("响应成功");
        return helloService.sayHello(name);
    }
}
