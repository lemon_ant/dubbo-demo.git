package com.hkrt.service;

/**
 * @author Surpass
 * @Package com.hkrt.service
 * @Description: ${todo}
 * @date 2020/10/27 15:52
 */
public interface HelloService {
    public String sayHello(String name);
}
